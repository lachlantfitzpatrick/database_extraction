/*

VITALS
Filename: MES Database Extraction_Scheduled Append_1.3.1.sql
Author: Lachlan Fitzpatrick
Date: 2015-02-19
Company: Burra Foods Australia
Version: 1.3.1


NOTES
Operatonal: Yes
Purpose: Pull append new data in the PI historian into a series of SQL tables.
Dependents:


VERSION HISTORY

1.0.0	Was capable of creating the views for tags specified in an 'IF' 
		statement.

1.1.0 	Use tables instead of views. Extra columns added.
1.1.1 	Fixed bug that duplicated the data.

1.2.0	Drop data off the table if it is older than a certain date.
1.2.1 	Merge new tags into the tag table rather than recreating the table.

1.3.0 	Restructured the data base to remove fields that were duplicated across
		all the records (ie. the tag name expansion columns).
1.3.1 	Bug fix for the tag name expansion and deadlocking. Removed the 
		trailing underscore	that was appearing after business areas with only 
		three letters. Set the deadlock priority to high.
		Included a test for whether the tag was an increment tag before 
		deleting negative data.


DOCUMENTATION


REFERENCES
Selecting a record in a Table: Pg 244 from 'Beginning T-SQL 2012' by Shaw, S. and Kellenberger, K.
Using a WHILE loop: Pg 254 from 'Beginning T-SQL 2012' by Shaw, S. and Kellenberger, K.
Using dynamic SQL: 	http://www.mssqltips.com/sqlservertip/1160/execute-dynamic-sql-commands-in-sql-server/
					http://www.databasejournal.com/features/mssql/article.php/1438931/Dynamic-SQL-Beginners-Guide.htm
					https://docs.oracle.com/cd/A57673_01/DOC/api/doc/PAD18/ch8.htm
View Execution Plan: http://msdn.microsoft.com/en-us/library/ms189562.aspx


FEATURES
The following is a list of useful techniques used in this code.

General Coding Techniques
	Declare variables ('DECLARE @sqlCmd VARCHAR(8000)');
	Set variables ('SET @tableName = 'dbo.vw_' + @currentTag');
	String concatenation ('SET @tableName = 'dbo.vw_' + @currentTag');
	If statement ('IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL BEGIN');
	While statement ('WHILE @counter <= @endCond BEGIN');
	Change the type of an object ('CONVERT(VARCHAR(50), CURRENT_TIMESTAMP)')

Language Specific Coding Techniques
	Drop (delete) a table ('DROP TABLE dbo.tbl_HistorianTags');
	Create a table ('CREATE TABLE dbo.tbl_HistorianTags (tag VARCHAR(255))');
	Populate a table using a 'SELECT' statement ('INSERT INTO dbo.tbl_HistorianTags');
	Count number of records in a column ('SELECT @endCond = COUNT(tag)');
	Assign a record from a table to a variable ('SELECT TOP (@counter) @currentTag = tag');
	Create a Dynamic SQL statement (a query that is executed in another query) ('SET @sqlCmd');
	Execute a Dynamic SQL statement ('EXEC (@sqlCmd)');
	Create a parametrised Dynamic SQL statement (a Dynamic SQL statement that takes input and gives output) ('SET @spSqlCmd');
	Execute a parametrised Dynamic SQL statement ('EXECUTE sp_executesql');
	Apply an operation to every row in a column ('[hour] = DATEPART(HOUR, [time])')


Useful Syntaxes and Functions
	Boolean check whether object exists ('IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL');
	Boolean check for similarities in text ('IF @currentTag LIKE 'BFA_%KVA%'');
	Boolean check for multiple conditions ('IF @currentTag LIKE 'BFA_%KVA%' OR @currentTag LIKE 'BFA_%KW%'');
	Select the top number of records in a table or table ('SELECT TOP 1000000');
	Select the unique records in a table or table ('SELECT  [tag]');
	Get the current time stamp ('CURRENT_TIMESTAMP');
	Get the largest value in a column ('SELECT MAX([time]');
	Get the index of a character in a string variable ('CHARINDEX('_', @currentTag)');
	Get a portion of a string based on index in the string ('SUBSTRING(@currentTag, 1, CHARINDEX('_', @currentTag)-1 )');
	Get a specific component of a date ('[hour] = DATEPART(HOUR, [time])')


*/

-------------------------------------------------------------------------------

----Specify database that will be worked in

USE MES

GO

-------------------------------------------------------------------------------

----Create the dbo.tbl_HistorianTags table

--Set the deadlock priority to ensure this query is killed on a deadlock.
SET DEADLOCK_PRIORITY LOW

--Check whether an object with name dbo.tbl_HistorianTags exists.
IF OBJECT_ID('dbo.tbl_HistorianTags') IS NULL BEGIN

	--Creates the tag table with name dbo.tbl_HistorianTags and columns 
	--tag, request, start_date and end_date.
	CREATE TABLE dbo.tbl_HistorianTags(tag VARCHAR(255), request VARCHAR(6), start_date VARCHAR(10), end_date VARCHAR(10), 
										site VARCHAR(10), business_area VARCHAR(10), business_unit VARCHAR(10), description VARCHAR(50))

END

--Merge any new tags into the table but leave the existing records alone. The
--merge was used instead of dropping the database as this is more efficient.
--Set the database to be merged into and give it a temporary name ('Target')
MERGE INTO dbo.tbl_HistorianTags AS Target
--Select the distinct tags from the PI database as the source.
USING (SELECT DISTINCT [tag] FROM [PI].[piarchive]..[picomp2]) AS Source
--Select the columns that are to be compared.
ON (Target.tag = Source.tag)
--When there is no tag name in the SQL database that matches and the source tag
--is in the new tag format then add the tag.
WHEN NOT MATCHED THEN
	INSERT (tag, request, start_date, end_date)
	VALUES (Source.tag, NULL, 'yyyy-mm-dd', 'yyyy-mm-dd');

--Create and execute the dynamic SQL statement string to populate the 
--tag name expansion columns.
--Declare the variable to hold the dynamic SQL statements.
DECLARE @sqlCmd VARCHAR(8000)
--For the site column get the text between the start and the first underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [site] = SUBSTRING([tag], 1, CHARINDEX(''_'', [tag])-1 ) 
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)
--For the business_area column get the text between the first and second underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [business_area] = SUBSTRING([tag], CHARINDEX(''_'', [tag])+1, CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag]))-1 ) 
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)
--For the business_unit column get the text between the second and third underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [business_unit] = REPLACE(SUBSTRING([tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag])+1)+1, CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag])))),''_'','''')
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)
--For the description column get the text after the thrid underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [description] = SUBSTRING([tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag])+1)+1)+1, LEN([tag])) 
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)

GO

-------------------------------------------------------------------------------

----Loop that pulls the data out of the PI database into the SQL tables.

--Set the deadlock priority.
SET DEADLOCK_PRIORITY LOW

--Declare the end condition variable.
DECLARE @endCond INT
--Declare the variable to hold the record from tag table.
DECLARE @currentTag VARCHAR(255)
--Declare the variable to hold the dynamic SQL statements.
DECLARE @sqlCmd VARCHAR(8000)
--Declare the variable to hold the table name.
DECLARE @tableName VARCHAR(255)
--Declare the variable to hold the latest data date.
DECLARE @latestDataTime DATETIME2
--Declare the variable to hold the latest data date.
DECLARE @currentTime DATETIME2
--Declare the variable to hold the sp_execute dynamic SQL statements.
DECLARE @spSqlCmd NVARCHAR(4000)
--Declare the variable to hold any parameters for sp_execeute dynamic SQL.
DECLARE @paramDefinition NVARCHAR(500)
--Declare the counter variable.
DECLARE @counter INT = 1

--Set the end condition for the WHILE loop as the length of the
--dbo.tbl_HistorianTags table.
--Count the number of tags in the tag table.
SELECT @endCond = COUNT(tag)
FROM dbo.tbl_HistorianTags

--A WHILE loop to use the records in dbo.tbl_HistorianTags as input for creating 
--a new table.
WHILE @counter <= @endCond BEGIN

	--Select the next record in dbo.tbl_HistorianTags (indicated by @counter) 
	--and assign it to @currentTag. 'TOP' statement iteratively assigns each 
	--entry in tag from the top to @currentTag until the limit @counter 
	--is reached.
	SELECT TOP (@counter) @currentTag = tag
	--Select the table to look at.
	FROM dbo.tbl_HistorianTags

	--Check if the current tag is in the correct format. Eg. starts with 'KOR_'.
	IF @currentTag LIKE 'KOR[_]%' BEGIN

		--Execute the code to create or update the table for this tag.
		--Create the table name.
		SET @tableName = 'dbo.tbl_' + @currentTag

		--Set the current time to now.
		SET @currentTime = CURRENT_TIMESTAMP

		--Alter the table to include the most recent data if the table already 
		--exists. This precedes the check for whether the table needs to be
		--created as this statement would also be executed after a new table
		--was created pointlessly duplicating the processing.
		IF OBJECT_ID(@tableName) IS NOT NULL BEGIN

			--Get the date of the most recent piece of data.
			--Create and execute the parametrised dynamic SQL statement to get
			--the date of the most recent data point.
			SET @spSqlCmd = N'SET @dynamicLatestDataDate = (SELECT MAX([time]) FROM ' + @tableName + ')'
			--Specify the parameters.
			SET @paramDefinition = N'@dynamicLatestDataDate DATETIME2 OUTPUT'
			EXECUTE sp_executesql @spSqlCmd, @paramDefinition, @dynamicLatestDataDate = @latestDataTime OUTPUT

			--Create and execute the dynamic SQL statement string to append
			--data to the table. Extra condition included to fix duplicate
			--data bug.
			SET @sqlCmd =	'INSERT INTO ' + @tableName + ' (tag, time, value, status)
							SELECT TOP 1000000 [tag], [time], [value], [status]
							FROM [PI].[piarchive]..[picomp2]
							WHERE [tag] = ''' + @currentTag + '''
							AND [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + '''
							AND [time] < ''' + CONVERT(VARCHAR(50), @currentTime) + '''
							AND [time] <> ''' + CONVERT(VARCHAR(50), @latestDataTime) + ''''
			EXEC (@sqlCmd)

		END

		--Create a new table if the table doesn't already exist.
		IF OBJECT_ID(@tableName) IS NULL BEGIN

			--Set the latest data date to be an hour ago so that we pull an hour
			--of data into the new table.
			SET @latestDataTime = DATEADD(HOUR, -1, @currentTime)
			
			--Create and execute the dynamic SQL statement string to create the 
			--new table.
			SET @sqlCmd = 	'CREATE TABLE ' + @tableName + '(tag VARCHAR(255), time VARCHAR(50), date VARCHAR(10), 
															hour VARCHAR(2), minute VARCHAR(2), second VARCHAR(2), 
															value VARCHAR(100), status VARCHAR(10))'
			EXEC (@sqlCmd)
			--Create and execute the dynamic SQL statement string to populate the
			--new table with the last hour of data.
			SET @sqlCmd =	'INSERT INTO ' + @tableName + ' (tag, time, value, status)
							SELECT TOP 1000000 [tag], [time], [value], [status]
							FROM [PI].[piarchive]..[picomp2]
							WHERE [tag] = ''' + @currentTag + '''
							AND [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + '''
							AND [time] < ''' + CONVERT(VARCHAR(50), @currentTime) + ''''
			EXEC (@sqlCmd)

		END

		--Check if the current tag is an increment tag.
		IF @currentTag LIKE 'KOR[_]%INCR%' BEGIN

			--Delete any negative values from the table.
			SET @sqlCmd = 	'DELETE FROM ' + @tableName + ' 
							WHERE [value] LIKE ''-%''
							AND [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + '''
							AND [time] < ''' + CONVERT(VARCHAR(50), @currentTime) + ''''
			EXEC (@sqlCmd)

		END

		--Create and execute the dynamic SQL statement string to populate the 
		--time expansion columns.
		--For the date column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [date] = CONVERT(DATE, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + ''''
		EXEC (@sqlCmd)
		--For the hour column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [hour] = DATEPART(HOUR, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + ''''
		EXEC (@sqlCmd)
		--For the minute column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [minute] = DATEPART(MINUTE, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + ''''
		EXEC (@sqlCmd)
		--For the second column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [second] = DATEPART(SECOND, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @latestDataTime) + ''''
		EXEC (@sqlCmd)

	END

	--Increment counter.
	SET @counter += 1

END

GO