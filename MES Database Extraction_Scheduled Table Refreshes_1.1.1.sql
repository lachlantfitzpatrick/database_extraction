/*

VITALS
Filename: MES Database Extraction_Scheduled Tag Table Refresh_1.0.1.sql
Author: Lachlan Fitzpatrick
Date: 2015-02-18
Company: Burra Foods Australia
Version: 1.0.1


NOTES
Operatonal: Yes
Purpose: Refresh all the tables of unwanted data.
Dependents:


VERSION HISTORY

1.1.0	Drops the tag table and recreates it and deletes unwanted values from
		the tag data tables.
1.1.1 	Bug fix for the tag name expansion and deadlocking. Removed the 
		trailing underscore	that was appearing after business areas with only 
		three letters. Set the deadlock priority to high.


DOCUMENTATION


REFERENCES


FEATURES
The following is a list of useful techniques used in this code.

General Coding Techniques
	If statement ('IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL BEGIN');	
	Declare variables ('DECLARE @sqlCmd VARCHAR(8000)');
	String concatenation ('SET @tableName = 'dbo.vw_' + @currentTag');

Language Specific Coding Techniques
	Drop (delete) a table ('DROP TABLE dbo.tbl_HistorianTags');
	Create a table ('CREATE TABLE dbo.tbl_HistorianTags (tag VARCHAR(255))');
	Populate a table using a 'SELECT' statement ('INSERT INTO dbo.tbl_HistorianTags');
	Create a Dynamic SQL statement (a query that is executed in another query) ('SET @sqlCmd');
	Execute a Dynamic SQL statement ('EXEC (@sqlCmd)')

Useful Syntaxes and Functions
	Boolean check whether object exists ('IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL');
	Select the unique records in a table or table ('SELECT  [tag]');
	Get the current time stamp ('CURRENT_TIMESTAMP')


*/

-------------------------------------------------------------------------------

----Specify database that will be worked in

USE MES

GO

-------------------------------------------------------------------------------

----Create the dbo.tbl_HistorianTags table

--Set the deadlock priority.
SET DEADLOCK_PRIORITY HIGH

--Declare the variable to hold the dynamic SQL statements.
DECLARE @sqlCmd VARCHAR(8000)

--Check whether an object with name dbo.tbl_HistorianTags exists.
IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL BEGIN
	--If the object exists then drop the table with name dbo.tbl_HistorianTags.
	DROP TABLE dbo.tbl_HistorianTags
END

--Creates the tag table with name dbo.tbl_HistorianTags and columns 
--tag, request, start_date and end_date.
CREATE TABLE dbo.tbl_HistorianTags(tag VARCHAR(255), request VARCHAR(6), start_date VARCHAR(10), end_date VARCHAR(10), 
									site VARCHAR(10), business_area VARCHAR(10), business_unit VARCHAR(10), description VARCHAR(50))

--Tells SQL that the data from the following SELECT is to be inserted into
--dbo.tbl_HistorianTags.
INSERT INTO dbo.tbl_HistorianTags (tag)
--Selects all unique tags from the Historian database.
SELECT DISTINCT [tag] FROM [PI].[piarchive]..[picomp2]

--Create and execute the dynamic SQL statement string to populate the special
--request columns.
--For the request column.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags SET [request] = NULL'
EXEC (@sqlCmd)
--For the start date column.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags SET [start_date] = ''yyyy-mm-dd'''
EXEC (@sqlCmd)
--For the end date column.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags SET [end_date] = ''yyyy-mm-dd'''
EXEC (@sqlCmd)
--Create and execute the dynamic SQL statement string to populate the 
--tag name expansion columns.
--For the site column get the text between the start and the first underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [site] = SUBSTRING([tag], 1, CHARINDEX(''_'', [tag])-1 ) 
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)
--For the business_area column get the text between the first and second underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [business_area] = SUBSTRING([tag], CHARINDEX(''_'', [tag])+1, CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag]))-1 ) 
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)
--For the business_unit column get the text between the second and third underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [business_unit] = REPLACE(SUBSTRING([tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag])+1)+1, CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag])))),''_'','''')
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)
--For the description column get the text after the thrid underscore.
SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags 
				SET [description] = SUBSTRING([tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag], CHARINDEX(''_'', [tag])+1)+1)+1, LEN([tag])) 
				WHERE [tag] LIKE ''KOR[_]%'''
EXEC (@sqlCmd)

GO

-------------------------------------------------------------------------------

----Loop that iterates through all the tag data tables.

--Set the deadlock priority.
SET DEADLOCK_PRIORITY HIGH

--Declare the end condition variable.
DECLARE @endCond INT
--Declare the variable to hold the record from tag table.
DECLARE @currentTag VARCHAR(255)
--Declare the variable to hold the dynamic SQL statements.
DECLARE @sqlCmd VARCHAR(8000)
--Declare the variable to hold the table name.
DECLARE @tableName VARCHAR(255)
--Declare the variable to hold the latest data date.
DECLARE @currentTime DATETIME2
--Declare the variable to hold the top entry in a table.
DECLARE @tableRows INT
--Declare the variable to hold the sp_execute dynamic SQL statements.
DECLARE @spSqlCmd NVARCHAR(4000)
--Declare the variable to hold any parameters for sp_execeute dynamic SQL.
DECLARE @paramDefinition NVARCHAR(500)
--Declare the counter variable.
DECLARE @counter INT = 1

--Set the end condition for the WHILE loop as the length of the
--dbo.tbl_HistorianTags table.
--Select the count function as the result to be assigned to @endCond and
--column and table to be used.
SELECT @endCond = COUNT(tag)
FROM dbo.tbl_HistorianTags

--A WHILE loop to use the records in dbo.tbl_HistorianTags as input for creating 
--a new table.
WHILE @counter <= @endCond BEGIN

	--Select the next record in dbo.tbl_HistorianTags (indicated by @counter) 
	--and assign it to @currentTag. 'TOP' statement iteratively assigns each 
	--entry in tag from the top to @currentTag until the limit @counter 
	--is reached.
	SELECT TOP (@counter) @currentTag = tag
	--Select the table to look at.
	FROM dbo.tbl_HistorianTags

	--Check if the current tag is a utility tag. Eg. contains 'KOR'.
	IF @currentTag LIKE 'KOR[_]%' BEGIN

		--Execute the code to create the table for this tag.
		--Create the table name.
		SET @tableName = 'dbo.tbl_' + @currentTag

		--If the table exists then delete the unwanted values.
		IF OBJECT_ID(@tableName) IS NOT NULL BEGIN

			--Set the current time to now.
			SET @currentTime = CURRENT_TIMESTAMP
			
			--Keep all other data for 2 years.
			IF (@currentTag LIKE 'KOR[_]%') BEGIN
				SET @sqlCmd = 'DELETE FROM ' + @tableName + ' 
				WHERE [time] < ''' + CONVERT(VARCHAR(50), DATEADD(MONTH, -24, @currentTime)) + ''''
				EXEC (@sqlCmd)
			END

			--Get the number of records in the table.
			SET @spSqlCmd = N'SET @dynamicTableRows = (SELECT COUNT(tag) FROM ' + @tableName + ')'
			--Specify the parameters.
			SET @paramDefinition = N'@dynamicTableRows INT OUTPUT'
			EXECUTE sp_executesql @spSqlCmd, @paramDefinition, @dynamicTableRows = @tableRows OUTPUT

			--If there is no data in the table delete it.
			IF @tableRows = 0 BEGIN

				--Drop the table.
				SET @sqlCmd = 'DROP TABLE ' + @tableName
				EXEC (@sqlCmd)

			END

		END

	END

	--Increment counter.
	SET @counter += 1

END

GO