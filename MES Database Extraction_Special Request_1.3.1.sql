/*

VITALS
Filename: MES Database Extraction_Special Request_1.3.1.sql
Author: Lachlan Fitzpatrick
Date: 2015-02-19
Company: Burra Foods Australia
Version: 1.3.1


NOTES
Operatonal: Yes
Purpose: Pull data in a given date range into the relevant table on request.
Dependents: Assumes that 'tbl_HistorianTags' has already been created;
			this query was based off the file 'MES Database Extraction_Scheduled Append_1.2.1.sql'.


VERSION HISTORY

1.0.0	Was capable of creating the views for tags specified in an 'IF' 
		statement.

1.1.0 	Use tables instead of views. Extra columns added.
1.1.1 	Fixed bug that duplicated the data.

1.2.0	Was repurposed to pull historical data by request.

1.3.0 	Restructured the data base to remove fields that were duplicated across
		all the records (ie. the tag name expansion columns).
1.3.1 	Bug fix for the deadlocking. Set the deadlock priority to normal.
		Included a test for whether the tag was an increment tag before 
		deleting negative data.



DOCUMENTATION


REFERENCES
Selecting a record in a Table: Pg 244 from 'Beginning T-SQL 2012' by Shaw, S. and Kellenberger, K.
Using a WHILE loop: Pg 254 from 'Beginning T-SQL 2012' by Shaw, S. and Kellenberger, K.
Using dynamic SQL: 	http://www.mssqltips.com/sqlservertip/1160/execute-dynamic-sql-commands-in-sql-server/
					http://www.databasejournal.com/features/mssql/article.php/1438931/Dynamic-SQL-Beginners-Guide.htm
					https://docs.oracle.com/cd/A57673_01/DOC/api/doc/PAD18/ch8.htm
View Execution Plan: http://msdn.microsoft.com/en-us/library/ms189562.aspx


FEATURES
The following is a list of useful techniques used in this code.

General Coding Techniques
	Declare variables ('DECLARE @sqlCmd VARCHAR(8000)');
	Set variables ('SET @tableName = 'dbo.vw_' + @currentTag');
	String concatenation ('SET @tableName = 'dbo.vw_' + @currentTag');
	If statement ('IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL BEGIN');
	While statement ('WHILE @counter <= @endCond BEGIN');
	Change the type of an object ('CONVERT(VARCHAR(50), CURRENT_TIMESTAMP)')

Language Specific Coding Techniques
	Drop (delete) a table ('DROP TABLE dbo.tbl_HistorianTags');
	Create a table ('CREATE TABLE dbo.tbl_HistorianTags (tag VARCHAR(255))');
	Populate a table using a 'SELECT' statement ('INSERT INTO dbo.tbl_HistorianTags');
	Count number of records in a column ('SELECT @endCond = COUNT(tag)');
	Assign a record from a table to a variable ('SELECT TOP (@counter) @currentTag = tag');
	Create a Dynamic SQL statement (a query that is executed in another query) ('SET @sqlCmd');
	Execute a Dynamic SQL statement ('EXEC (@sqlCmd)');
	Create a parametrised Dynamic SQL statement (a Dynamic SQL statement that takes input and gives output) ('SET @spSqlCmd');
	Execute a parametrised Dynamic SQL statement ('EXECUTE sp_executesql');
	Apply an operation to every row in a column ('[hour] = DATEPART(HOUR, [time])')


Useful Syntaxes and Functions
	Boolean check whether object exists ('IF OBJECT_ID('dbo.tbl_HistorianTags') IS NOT NULL');
	Boolean check for similarities in text ('IF @currentTag LIKE 'BFA_%KVA%'');
	Boolean check for multiple conditions ('IF @currentTag LIKE 'BFA_%KVA%' OR @currentTag LIKE 'BFA_%KW%'');
	Select the top number of records in a table or table ('SELECT TOP 1000000');
	Select the unique records in a table or table ('SELECT  [tag]');
	Get the current time stamp ('CURRENT_TIMESTAMP');
	Get the largest value in a column ('SELECT MAX([time]');
	Get the index of a character in a string variable ('CHARINDEX('_', @currentTag)');
	Get a portion of a string based on index in the string ('SUBSTRING(@currentTag, 1, CHARINDEX('_', @currentTag)-1 )');
	Get a specific component of a date ('[hour] = DATEPART(HOUR, [time])')


*/

-------------------------------------------------------------------------------

----Specify database that will be worked in

USE MES

GO

-------------------------------------------------------------------------------

----Loop that pulls the data out of the PI database into the SQL tables.

--Set the deadlock priority.
SET DEADLOCK_PRIORITY NORMAL

--Declare the end condition variable.
DECLARE @endCond INT
--Declare the variable to hold the record from tag table.
DECLARE @currentTag VARCHAR(255)
--Declare the variable to hold the dynamic SQL statements.
DECLARE @sqlCmd VARCHAR(8000)
--Declare the variable to hold the table name.
DECLARE @tableName VARCHAR(255)
--Declare the variable to hold the request status.
DECLARE @requestStatus VARCHAR(6)
--Declare the variable to hold the start date.
DECLARE @startDate VARCHAR(10)
--Declare the variable to hold the end date.
DECLARE @endDate VARCHAR(10)
--Declare the counter variable.
DECLARE @counter INT = 1

--Set the end condition for the WHILE loop as the length of the
--dbo.tbl_HistorianTags table.
--Select the count function as the result to be assigned to @endCond and
--column and table to be used.
SELECT @endCond = COUNT(tag)
FROM dbo.tbl_HistorianTags

--WHILE loop to use every record in dbo.tbl_HistorianTags as input for creating 
--a new table.
WHILE @counter <= @endCond BEGIN

	--Select the next record in dbo.tbl_HistorianTags (indicated by @counter) 
	--and assign the tag name to @currentTag and request status to 
	--@requestStatus. 'TOP' statement iteratively assigns each entry in 
	--tag from the top to @currentTag until the limit @counter is reached.
	SELECT TOP (@counter) @currentTag = tag, @requestStatus = request
	--Select the table to look at.
	FROM dbo.tbl_HistorianTags

	--Check if the current tag is a utility tag. Eg. contains 'KOR'.
	IF @currentTag LIKE 'KOR[_]%' AND (@requestStatus <> NULL OR @requestStatus <> 'FAILED' OR @requestStatus <> '') BEGIN
		
		--Select the start and end dates.
		SELECT TOP (@counter) @startDate = start_date, @endDate = end_date
		--Select the table to look at.
		FROM dbo.tbl_HistorianTags

		--Check whether the start date is before the end date. If it is then
		--they need to be swapped.
		IF @startDate > @endDate BEGIN

			--Select the start and end dates.
			SELECT TOP (@counter) @startDate = end_date, @endDate = start_date
			--Select the table to look at.
			FROM dbo.tbl_HistorianTags

		END 

		--Execute the code to create the table for this tag.
		--Create the table name.
		SET @tableName = 'dbo.tbl_' + @currentTag

		--Alter the table to include the most recent data if the table already 
		--exists. This precedes the check for whether the table needs to be
		--created as this statement would also be executed after a new table
		--was created pointlessly duplicating the processing.
		IF OBJECT_ID(@tableName) IS NOT NULL BEGIN

			--Create and execute the dynamic SQL statement string to merge
			--data into the table. The merge statement prevents duplicates in
			--the SQL database.
			SET @sqlCmd =	'MERGE INTO ' + @tableName + ' AS Target
							USING (
							SELECT TOP 1000000 [tag], [time], [value], [status]
							FROM [PI].[piarchive]..[picomp2]
							WHERE [tag] = ''' + @currentTag + '''
							AND [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
							AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + '''
							) AS Source
							ON (Target.Time = Source.Time)
							WHEN NOT MATCHED THEN
								INSERT (tag, time, value, status)
								VALUES (Source.tag, Source.time, Source.value, Source.status);'
			EXEC (@sqlCmd)

		END

		--Create a new table if the table doesn't already exist.
		IF OBJECT_ID(@tableName) IS NULL BEGIN
			
			--Create and execute the dynamic SQL statement string to create the 
			--new table.
			SET @sqlCmd = 	'CREATE TABLE ' + @tableName + '(tag VARCHAR(255), time VARCHAR(50), date VARCHAR(10), 
															hour VARCHAR(2), minute VARCHAR(2), second VARCHAR(2), 
															value VARCHAR(100), status VARCHAR(10))'
			EXEC (@sqlCmd)
			--Create and execute the dynamic SQL statement string to merge
			--data into the table. The merge statement prevents duplicates in
			--the SQL database.
			SET @sqlCmd =	'MERGE INTO ' + @tableName + ' AS Target
							USING (
							SELECT TOP 1000000 [tag], [time], [value], [status]
							FROM [PI].[piarchive]..[picomp2]
							WHERE [tag] = ''' + @currentTag + '''
							AND [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
							AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + '''
							) AS Source
							ON (Target.Time = Source.Time)
							WHEN NOT MATCHED THEN
								INSERT (tag, time, value, status)
								VALUES (Source.tag, Source.time, Source.value, Source.status);'
			EXEC (@sqlCmd)

		END

		--Check if the current tag is an increment tag.
		IF @currentTag LIKE 'KOR[_]%INCR%' BEGIN

			--Delete any negative values from the table.
			SET @sqlCmd = 	'DELETE FROM ' + @tableName + ' 
							WHERE [value] LIKE ''-%''
							AND [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
							AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + ''''
			EXEC (@sqlCmd)

		END

		--Create and execute the dynamic SQL statement string to populate the 
		--time expansion columns.
		--For the date column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [date] = CONVERT(DATE, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
						AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + ''''
		EXEC (@sqlCmd)
		--For the hour column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [hour] = DATEPART(HOUR, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
						AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + ''''
		EXEC (@sqlCmd)
		--For the minute column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [minute] = DATEPART(MINUTE, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
						AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + ''''
		EXEC (@sqlCmd)
		--For the second column.
		SET @sqlCmd = 	'UPDATE ' + @tableName + ' 
						SET [second] = DATEPART(SECOND, [time])
						WHERE [time] > ''' + CONVERT(VARCHAR(50), @startDate + ' 00:00:00.0000000') + '''
						AND [time] < ''' + CONVERT(VARCHAR(50), @endDate + ' 23:59:59.9999999') + ''''
		EXEC (@sqlCmd)

		--Remove the request flag (the 'Yes' in the 'request' column) and reset
		--the date columns.
		SET @sqlCmd = 	'UPDATE dbo.tbl_HistorianTags SET [request] = NULL WHERE [tag] = ''' + @currentTag + ''''
		EXEC (@sqlCmd)

	END

	--Increment counter.
	SET @counter += 1

END

GO